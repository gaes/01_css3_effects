# CSS3 #

CSS3 is an abbreviation for Cascading Style Sheets, level 3, a declarative stylesheet language for structured documents.

CSS3 has been split into "modules". It contains the "old CSS specification" (which has been split into smaller pieces). In addition, new modules are added.

Some of the most important CSS3 modules are:

* Selectors
* Box Model
* Backgrounds and Borders
* Image Values and Replaced Content
* Text Effects
* 2D/3D Transformations
* Animations
* Multiple Column Layout
* User Interface


https://www.w3schools.com/css/css3_borders.asp

http://www.newthinktank.com/2015/04/learn-css3-one-video/